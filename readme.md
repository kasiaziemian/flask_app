# Flask app 
Application allows user to make account and profile, post content on page and follow other users. I used Docker Containers for deployment, Celery to handle background tasks, Elasticsearch for searching page content, MySQL database and SQLAlchemy ORM. I also built simple API to work directly with the application's resources. 

# Run
 ```
$ cd flask_app
$ source venv/bin/activate
$ python -m pip install -r requirements.txt
$ FLASK_APP=main.py
$ flask db upgrade
$ docker-compose up
 ```



