FROM python:3.6-alphine

ENV FLASK_APP main.py

COPY ./requirements.txt /requiremnents.txt

RUN pip install -r / requirements.txt

RUN md /flask_app
WORKDIR /flask_app
COPY . /flask_app 

RUN adduser -D user 
USER user 
